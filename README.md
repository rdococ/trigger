Trigger
=======

Trigger mod for Minetest, by rdococ. This mod was mainly inspired by the mech mod, which is part of the insidethebox game.

Mechanics
---------

The trigger mod adds a system of wireless signals. You may use the connector tool to connect one node to another. This means that any signal that the former node outputs, will be given to the latter node (specifically, its on_trigger callback will be called with the latter's position, node table, and trigger value). The trigger value may either be ON, or OFF.

Connector
---------

The connector tool, whose internal name is simply "trigger:tool", is used for two purposes:
1. Left-click a block to begin a connection, or complete it if one has already begun. When a connection is made, the node at the beginning of the connection will now send its result to the recipient. A node may be connected to multiple recipients at a time, and may also be the recipient of multiple connections.
2. Right-click a block to see a list of recipients it can send a signal to. Note that nodes cannot select only one of these recipients to signal to; the node must either not send a signal, or send one to all of its recipients, for simplicity's sake.

Compatibility
-------------

Pistons and doors can be opened by an ON signal and closed by an OFF signal, with the latter being openable regardless of the presence of mesecons. The nodes store their recipients in metadata as relative coordinates, meaning that you can use WE to move an entire machine, but not one part of it (if it connects to other parts).

Helper nodes
------------

There are multiple helper nodes, mainly based on the ones from the mech mod, which perform various tasks on received signals and then send the results out.
1. The delayer node delays a signal for the specified number of seconds. Consecutive signals are handled properly.
2. The extender node only delays OFF signals. ON signals are passed immediately. Similarly to the delayer, consecutive signals are handled properly.
3. The filter node completely ignores OFF signals, but ON signals are passed.
4. The inverter node converts an ON signal to an OFF signal, and vice versa.
5. The adder node counts the number of ON signals minus the number of OFF signals. When that count increases to a user-specified limit, it generates an ON signal. If the count decreases back below the limit, it generates an OFF signal.
