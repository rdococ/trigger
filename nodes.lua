--[[
Nodes to aid in creating trigger-based machinery

extender: takes in a number t. when it receives an ON trigger, it will just send it straight on. when it receives an OFF trigger, it will wait for t seconds before sending it through.
	ON -> ON
	OFF -> OFF t seconds later

filter: passes ON signals only.
	ON -> ON
	OFF -> no signal

pulser: takes in a number t. when it receives an ON trigger, it will pass it through and then generate an OFF trigger t seconds later.
	ON -> ON, OFF t seconds later
	OFF -> no signal

inverter: turns an ON trigger into an OFF trigger, and vice versa.
	ON -> OFF
	OFF -> ON

adder: takes in a number n. counts the number of ON triggers, minus the number of OFF triggers. when this number increases to n, send an ON trigger. when the number decreases to n-1, send an OFF trigger. set n to 1 for an OR gate, or the number of inputs used for an AND gate.
]]

minetest.register_node("trigger:adder", {
	description = "Trigger: Adder",
	tiles = {"trigger_adder.png"},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[required_count;Required number of ON signals;${required_count}]")
	end,
	on_trigger = function (pos, node, value)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		
		local old_count = meta:get_int("current_count") or 0
		current_count = old_count + (value and 1 or -1)
		
		meta:set_int("current_count", current_count)
		if old_count < meta:get_int("required_count") and current_count >= meta:get_int("required_count") then
			-- The old count was below the required limit; the new one is. Send an ON signal.
			trigger.send(pos, true)
		elseif old_count >= meta:get_int("required_count") and current_count < meta:get_int("required_count") then
			-- The old count was at or above the limit; the new one is below. Send an OFF signal.
			trigger.send(pos, false)
		end
	end,
	on_receive_fields = function(pos, formname, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if fields.required_count then
			minetest.get_meta(pos):set_string("required_count", fields.required_count)
		end
	end
})

minetest.register_node("trigger:delayer", {
	description = "Trigger: Delayer",
	tiles = {"trigger_delayer.png"},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[delay;Delay in seconds;${delay}]")
	end,
	on_trigger = function (pos, node, value)
		local meta = minetest.get_meta(pos)
		minetest.after(meta:get_int("delay") or 1, function (pos, value) trigger.send(pos, value) end, pos, value)
	end,
	on_receive_fields = function(pos, formname, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if fields.delay then
			minetest.get_meta(pos):set_string("delay", fields.delay)
		end
	end
})

minetest.register_node("trigger:extender", {
	description = "Trigger: Extender",
	tiles = {"trigger_extender.png"},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[delay;Delay in seconds;${delay}]")
	end,
	on_trigger = function (pos, node, value)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		
		if value then
			trigger.send(pos, value)
		else
			minetest.after(meta:get_int("delay") or 1, function (pos, value) trigger.send(pos, value) end, pos, value)
		end
	end,
	on_receive_fields = function(pos, formname, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if fields.delay then
			minetest.get_meta(pos):set_string("delay", fields.delay)
		end
	end
})
minetest.register_node("trigger:filter", {
	description = "Trigger: Filter",
	tiles = {"trigger_filter.png"},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_trigger = function (pos, node, value)
		if value then
			trigger.send(pos, value)
		end
	end
})

minetest.register_node("trigger:pulser", {
	description = "Trigger: Pulser",
	tiles = {"trigger_pulser.png"},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", "field[delay;Pulse length;${delay}]")
	end,
	on_trigger = function (pos, node, value)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		
		if value then
			trigger.send(pos, value)
			minetest.after(meta:get_int("delay") or 1, function (pos, value) trigger.send(pos, not value) end, pos, value)
		end
	end,
	on_receive_fields = function(pos, formname, fields, sender)
		local name = sender:get_player_name()
		if minetest.is_protected(pos, name) and not minetest.check_player_privs(name, {protection_bypass=true}) then
			minetest.record_protection_violation(pos, name)
			return
		end
		if fields.delay then
			minetest.get_meta(pos):set_string("delay", fields.delay)
		end
	end
})

minetest.register_node("trigger:inverter", {
	description = "Trigger: Inverter",
	tiles = {"trigger_inverter.png"},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_trigger = function (pos, node, value)
		trigger.send(pos, not value)
	end
})
