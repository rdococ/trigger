local button_size = {x = 4, y = 2.5}
local pplate_size = {x = 15, y = 1, z = 15}
local switch_size = {x = 2, y = 2}

minetest.register_node("trigger:button_off", {
	description = "Trigger: Button",
	tiles = {"trigger_button.png"},
	
	paramtype = "light",
	paramtype2 = "facedir",
	
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{ -button_size.x/16, -button_size.y/16, 5/16, button_size.x/16, button_size.y/16, 8/16 },
		}
	},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_rightclick = function (pos, node)
		trigger.send(pos, true)
		minetest.swap_node(pos, {name="trigger:button_on", param2=minetest.get_node(pos).param2})
		minetest.get_node_timer(pos):start(1)
	end
})
minetest.register_node("trigger:button_on", {
	description = "Trigger: Button (on)",
	tiles = {"trigger_button.png"},
	
	paramtype = "light",
	paramtype2 = "facedir",
	
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{ -button_size.x/16, -button_size.y/16, 6.5/16, button_size.x/16, button_size.y/16, 8/16 },
		}
	},
	
	groups = {oddly_breakable_by_hand = 1},
	drop = "trigger:button_off",
	
	on_timer = function(pos, elapsed)
		trigger.send(pos, false)
		minetest.swap_node(pos, {name="trigger:button_off", param2=minetest.get_node(pos).param2})
		return false
	end,
	on_rightclick = function (pos, node)
		-- this prevents accidental node placement
	end
})

minetest.register_node("trigger:switch_off", {
	description = "Trigger: Switch",
	tiles = {"trigger_texture.png"},
	
	paramtype = "light",
	paramtype2 = "facedir",
	
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{ -switch_size.x/16, -switch_size.y/8, 7/16, switch_size.x/16, switch_size.y/8, 8/16 },
			{ -switch_size.x/16, 0/16, 5/16, switch_size.x/16, switch_size.y/8, 8/16 },
		}
	},
	
	groups = {oddly_breakable_by_hand = 1},
	
	on_rightclick = function (pos, node)
		trigger.send(pos, true)
		minetest.swap_node(pos, {name="trigger:switch_on", param2=minetest.get_node(pos).param2})
	end
})
minetest.register_node("trigger:switch_on", {
	description = "Trigger: Switch (on)",
	tiles = {"trigger_texture.png"},
	
	paramtype = "light",
	paramtype2 = "facedir",
	
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{ -switch_size.x/16, -switch_size.y/8, 7/16, switch_size.x/16, switch_size.y/8, 8/16 },
			{ -switch_size.x/16, -switch_size.y/8, 5/16, switch_size.x/16, 0/16, 8/16 },
		}
	},
	
	groups = {oddly_breakable_by_hand = 1},
	drop = "trigger:switch_off",
	
	on_rightclick = function (pos, node)
		trigger.send(pos, false)
		minetest.swap_node(pos, {name="trigger:switch_off", param2=minetest.get_node(pos).param2})
	end
})
