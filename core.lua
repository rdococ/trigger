--[[
BASIC CONCEPT:
when a node is activated (e.g. lever flipped, button pushed), it sends an ON trigger. when it is deactivated (e.g. lever flipped back, button released), it sends an OFF trigger.

A node may choose to send a trigger out with trigger.send(pos, value). value should be true for ON, and false for OFF. It will send the trigger out to the nodes that it's connected to.

NOTES:
Positions are stored relatively for portability reasons.
]]

--[[ super hacky hack of hacky hackiness
-- will remove eventually
if not minetest.get_modpath("on_swap") then
	local old_swap = minetest.swap_node
	minetest.swap_node = function (pos, ...)
		local r = old_swap(pos, ...)
	
		local node = minetest.get_node(pos)
		local def = minetest.registered_nodes[node.name]
	
		if def and def.on_swap then
			def.on_swap(pos, node)
		end
	
		return r
	end
end]]

local queue_limit = tonumber(minetest.setting_get("trigger.queue_limit") or 64)  -- maximum items the main execution queue should hold.

trigger = {}
_trigger = {}  -- these local variables are declared as such in init.lua
_connecting = {}

_trigger.queue = {}  -- We need a global queue to avoid stack overflow.
_trigger.limit_warning_issued = false

trigger.get = function (pos)
	return minetest.deserialize(minetest.get_meta(pos):get_string("trigger:connections")) or {}
end
trigger.set = function (pos, data)
	minetest.get_meta(pos):set_string("trigger:connections", minetest.serialize(data or {}))
end

trigger.connect = function (posa, posb)
	local cons = trigger.get(posa)
	-- table.insert(cons, vector.subtract(posb, posa))
	cons[vector.subtract(posb, posa)] = true
	trigger.set(posa, cons)
end
trigger.clear = function (pos)
	trigger.set(pos, {})
end

trigger.receive = function (pos, value)
	local node = minetest.get_node(pos)
	local def = minetest.registered_nodes[node.name]
	if not def then return end
	
	if def.on_trigger then
		def.on_trigger(pos, node, value)
	elseif def.mesecons and def.mesecons.effector then
		-- Support mesecons by detecting the effector table of the recipient,
		-- and simulating the activation/deactivation of the block.
		if value then
			if def.mesecons.effector.action_on then
				def.mesecons.effector.action_on(pos, node)
			end
		else
			if def.mesecons.effector.action_off then
				def.mesecons.effector.action_off(pos, node)
			end
		end
	end
end
trigger.send = function (pos, value)
	local cons = trigger.get(pos)
	for posb,_ in pairs(cons) do
		if #_trigger.queue > queue_limit then
			if not _trigger.limit_warning_issued then
				print("[Trigger] Warning: many trigger items in execution queue, preventing the creation of more.")
				_trigger.limit_warning_issued = true
			end
			return
		end
		_trigger.limit_warning_issued = _trigger.limit_warning_issued and (#_trigger.queue >= queue_limit - 4)
		table.insert(_trigger.queue, {pos = vector.add(pos, posb), value = value})
	end
end

_trigger.override_door = function (name)
	minetest.override_item(name, {
		on_trigger = function (pos, node, value)
			local meta = minetest.get_meta(pos)
			
			if value then
				doors.get(pos):open()
				return
			end
			doors.get(pos):close()
		end
	})
end

_trigger.convert = function (name, def)
	if def.on_trigger then return end
	if def.groups.door then
		_trigger.override_door(name)
	--[[elseif mesecon and def.mesecons and def.mesecons.receptor then
		if def.mesecons.receptor.state == mesecon.state.on then
			minetest.override_item(name, {
				on_swap = function (pos)
					trigger.send(pos, true)
				end
			})
		else
			minetest.override_item(name, {
				on_swap = function (pos)
					trigger.send(pos, false)
				end
			})
		end]]
	end
end

for name,def in pairs(minetest.registered_nodes) do
	_trigger.convert(name, def)
end
local old_register_node = minetest.register_node
minetest.register_node = function (...)
	local r = old_register_node(...)
	if r then
		_trigger.convert(...)
	end
	return r
end



minetest.register_globalstep(function (dtime)
	if #_trigger.queue < 1 then return end
	
	local first = _trigger.queue[1]
	table.remove(_trigger.queue, 1)
	
	trigger.receive(first.pos, first.value)
end)


minetest.register_craftitem("trigger:tool", {
	description = "Trigger: Connector tool",
	inventory_image = "trigger_tool.png",
	wield_image = "trigger_tool.png",
	on_use = function (itemstack, player, pointed_thing)
		if pointed_thing.type ~= "node" then return end
		local pos = pointed_thing.under
		local name = player:get_player_name()
		
		if minetest.is_protected(pos, name) then
			if _connecting[name] then
				minetest.chat_send_player(name, "Cannot connect to a protected area.")
			else
				minetest.chat_send_player(name, "Cannot connect a protected area.")
			end
			return
		end
		
		local display_pos = "(" .. pos.x .. ", " .. pos.y .. ", " .. pos.z .. ")"
		
		if _connecting[name] then
			minetest.chat_send_player(name, "...to " .. minetest.get_node(pos).name .. " at pos " .. display_pos)
			trigger.connect(_connecting[name], pos)
			_connecting[name] = nil
		else
			minetest.chat_send_player(name, "Connecting " .. minetest.get_node(pos).name .. " at pos " .. display_pos .. "...")
			_connecting[name] = pos
		end
	end,
	on_place = function (itemstack, player, pointed_thing)
		if pointed_thing.type ~= "node" then return end
		local pos = pointed_thing.under
		local name = player:get_player_name()
		
		if minetest.is_protected(pos, name) then
			minetest.chat_send_player(name, "Cannot read the connections of nodes in a protected area.")
			return
		end
		minetest.chat_send_player(name, "Connections:")
		
		local t = trigger.get(pos)
		local list = {}
		for i,_ in pairs(t) do
			local i = vector.add(pos, i)
			local display_pos = "(" .. i.x .. ", " .. i.y .. ", " .. i.z .. ")"
			minetest.chat_send_player(name, "to " .. minetest.get_node(i).name .. " at pos " .. display_pos)
		end
	end
})
