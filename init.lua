--[[
Trigger mod, designed for RD-C.
inspired by the insidethebox game's mod, mech.

The trigger mod adds a wireless signalling system, allowing inputs, such as buttons and switches, to interact with nodes such as doors and pistons from afar. The way it works is similar to the aforementioned mech mod.

There are two types of signal (technically, you could send any Lua object as a signal, but that is for other mods):
	- The ON signal, represented by the Lua value of true. When sent to output nodes such as doors and pistons, activates them.
	- The OFF signal, the inversion of the above. It deactivates output nodes - or, rather, calls their on_trigger callback with a value of false.
]]
local modpath = minetest.get_modpath(minetest.get_current_modname())
local _trigger
local _connecting
print("[Trigger] Loading core...")
dofile(modpath .. "/core.lua")	-- The main bulk of the mod; only this is technically required for trigger blocks to function.
print("[Trigger] Loading helper nodes...")
dofile(modpath .. "/nodes.lua")	-- Adds nodes to modify the properties of pulses and signals.
print("[Trigger] Loading input nodes...")
dofile(modpath .. "/input.lua")	-- Adds methods for the player to send a signal.
print("[Trigger] All set!")
